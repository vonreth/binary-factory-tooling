import json
import yaml
import os
import re
import requests
import zipfile
import multiprocessing
import subprocess
import xml.etree.ElementTree as ET
import xdg.DesktopEntry
import tempfile

dest = "repo"
# if dest directory doesn't exist, run fdroid init
# https://f-droid.org/en/docs/Setup_an_F-Droid_App_Repo/

#aaptCall = "docker-kde-android.sh /opt/android-sdk/build-tools/21.1.2/aapt dump badging /pwd/%s"
aaptCall = "aapt dump badging %s"
#fdroid = "docker run --rm -u $(id -u):$(id -g) -v $(pwd):/repo registry.gitlab.com/fdroid/docker-executable-fdroidserver:latest"
fdroid = "fdroid"

def readAppName(apkpath):
    manifest = subprocess.check_output(aaptCall % (apkpath), shell=True).decode('utf-8')
    result = re.search(' name=\'([^\']*)\'', manifest)
    return result.group(1)

def readText(elem, found):
    lang = elem.get('{http://www.w3.org/XML/1998/namespace}lang')

    if not lang in found:
        found[lang] = ""

    if elem.text:
        found[lang] += elem.text
        for child in elem:
            readText(child, found)

conv = {
    None: "en-US",
    "ca": "ca-ES"
}
def createFastlaneFile(appname, filename, locales):
    for lang, text in locales.items():
        path = 'metadata/' + appname + '/' + conv.get(lang, lang)
        os.makedirs(path, exist_ok=True)
        with open(path + '/' + filename, 'w') as f:
            f.write(text)

def createYml(appname, data):
    info = {}
    path = 'metadata/' + appname + '.yml'
    with open(path, 'r') as contents:
        info = yaml.load(contents)

    if 'categories' in data and data['categories'] != []:
        info['Categories'] = data['categories']
    else:
        info['Categories'] = ['KDE']

    info['Summary'] = data['summary'][None]
    with open(path, 'w') as output:
        yaml.dump(info, output, default_flow_style=False)


def appdataContents(apkpath, name):
    with zipfile.ZipFile(apkpath, 'r') as contents:

        #https://github.com/fastlane/fastlane/blob/2.28.7/supply/README.md#images-and-screenshots
        #https://docs.fastlane.tools/actions/supply/

        data = {}
        with contents.open("assets/share/metainfo/%s.appdata.xml" % name) as appdataFile:
            root = ET.fromstring(appdataFile.read())
            for child in root:
                output = {}
                if child.tag in data:
                    output = data[child.tag]
                readText(child, output)
                data[child.tag] = output


        with contents.open("assets/share/applications/%s.desktop" % name) as desktopFileContents:
            (fd, path) = tempfile.mkstemp(suffix=name + ".desktop")
            handle = open(fd, "wb")
            handle.write(desktopFileContents.read())
            handle.close()

            desktopFile = xdg.DesktopEntry.DesktopEntry(path)
            data['categories'] = desktopFile.getCategories()

        try:
            createFastlaneFile(name, "title.txt", data['name'])
            createFastlaneFile(name, "short_description.txt", data['summary'])
            createFastlaneFile(name, "full_description.txt", data['description'])
            createYml(name, data)
        except KeyError as e:
            print("error: key not found", e)


def fetch(job):
    name = job['name']

    zipfilename = "archive-%s.zip" % name
    with open(zipfilename, mode='wb') as f:
        url = "https://binary-factory.kde.org/view/Android/job/%s_android/lastSuccessfulBuild/artifact/*zip*/archive.zip" % name
        print("getting...", url)

        response = requests.get(url)
        data = response.content
        f.write(data)
        print("written", len(data), zipfilename)

    with zipfile.ZipFile(zipfilename, 'r') as contents:
        for entry in contents.namelist():
            if entry.endswith('-release-signed.apk'):
                filename = dest + '/' + entry[entry.rfind('/')+1:]
                with open(filename, mode='wb') as f:
                    f.write(contents.read(entry))
                print("created", filename)

                return filename
    print("could not find apk for %s" % (name) )
    return None

def decorate(filename):
    if not filename:
        return
    print("doing...", filename)
    appname = readAppName(filename)
    try:
        appdataContents(filename, appname)
    except KeyError as e:
        print("could not inspect", appname, e)

with open("current-jobs.json") as f:
    jobs = json.load(f)

    pool = multiprocessing.Pool(8)
    filenames = pool.map(fetch, jobs)

    subprocess.check_call(fdroid + " update -c", shell=True)

    pool.map(decorate, filenames)

    subprocess.check_call(fdroid + " update", shell=True)
    subprocess.check_call(fdroid + " server update -v", shell=True)



